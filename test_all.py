import unittest
from java_to_struct import *


class TestSubFunc(unittest.TestCase):
    MAINCLASS = Space(None, Class('class SubProduct', '\n * Бесполезный \n'),
                      40, 347)

    def test_bounds_first(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = self.MAINCLASS.start
            self.assertEqual(Parser(f.read()).get_bounds(self.MAINCLASS),
                             (131, 166))

    def test_bounds_second(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = 166
            self.assertEqual(Parser(f.read()).get_bounds(self.MAINCLASS),
                             (211, 238))

    def test_name_func(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = self.MAINCLASS.start
            self.assertEqual(
                Parser(f.read()).get_name(self.MAINCLASS).group('name'),
                'public static int Get')

    def test_name_class(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = 238
            self.assertEqual(
                Parser(f.read()).get_name(self.MAINCLASS).group('name'),
                'class Another')

    def test_get_blocks(self):
        self.assertEqual(Parser('cds {{}}').blocks, {4: 7, 5: 6, 7: 4, 6: 5})

    def test_get_blocks_empty(self):
        self.assertEqual(Parser('').blocks, dict())

    def test_doc(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = self.MAINCLASS.start
            self.assertEqual(Parser(f.read()).get_doc(self.MAINCLASS, 132),
                             ' * Еще более бесполезгая ф-ия ')

    def test_start(self):
        with open('./tests/test2.java', encoding='utf-8') as f:
            parser = Parser(f.read())
            parser.start()
            expected = Space(None,
                             Function('public static void Set', '(int count)',
                                      '\n * Что-то про set\n'),
                             59, 78)
            self.assertTrue(parser.result[0] == expected)


class TestCleaner(unittest.TestCase):
    EXPECTED_COMMENT = "\nclass Product\n{\n    \n    public int count;\n}"

    EXPECTED_BRACKETS = """class Product\n{\n    public int Get(){
        if( 10 > 20)\n\n        return 12;\n    }\n}"""

    def test_multiline_comment(self):
        with open('./tests/multiline_comment.java', encoding='utf-8') as f:
            result = Parser('').text_clean_comment(f.read())
            self.assertEqual(self.EXPECTED_COMMENT, result)

    def test_single_line_comment(self):
        with open('./tests/single_line_comment.java', encoding='utf-8') as f:
            result = Parser('').text_clean_comment(f.read())
            self.assertEqual(self.EXPECTED_COMMENT, result)

    def test_all_comment(self):
        with open('./tests/all_comment.java', encoding='utf-8') as f:
            result = Parser('').text_clean_comment(f.read())
            self.assertEqual(self.EXPECTED_COMMENT, result)

    def test_multiline_brackets(self):
        with open('./tests/multiline_brackets.java', encoding='utf-8') as f:
            p = Parser(f.read())
            result = p.text
            self.assertEqual(self.EXPECTED_BRACKETS, result)

    def test_single_line_brackets(self):
        with open('./tests/single_line_brackets.java', encoding='utf-8') as f:
            p = Parser(f.read())
            result = p.text
            self.assertEqual(self.EXPECTED_BRACKETS, result)
