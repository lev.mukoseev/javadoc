import argparse
import java_to_struct as jts
import struct_to_html as sth


def arg_parser():
    description = "Compiles Java Code to HTML Documentation"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('file_name', type=str, help="File with Java code")
    parser.add_argument("--out", default='./documentation.html',
                        help="Path to the output documentation")
    parser.add_argument('--console', action='store_true',
                        help='Print result to the console')
    return parser.parse_args()


if __name__ == '__main__':
    args = arg_parser()
    struct = jts.get_struct(args.file_name)
    struct.reverse()
    with open(args.out, 'w+') as f:
        f.write(f"<!DOCTYPE html>\n<html>\n<h1>{args.file_name}</h1>")
        sth.write(struct, f)
    if args.console:
        for namespace in struct:
            print(str(namespace))
