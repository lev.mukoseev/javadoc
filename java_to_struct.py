import re

ACCESS_MODES = {'private', 'public', 'protected', 'package-private'}


class Class:
    def __init__(self, name: str, doc: str):
        self.name = ''
        self.access_mode = ''
        self.extends = None
        self.implements = None
        self.docx = doc
        self.parse_name(name)

    def __eq__(self, other):
        return self.name == other.name and self.docx == other.docx

    def __str__(self):
        return (f'{self.access_mode} class {self.name}\n'
                + f'extends {self.extends}\n'
                + f'implements {self.implements}')

    def parse_name(self, name):
        name = re.sub(r'\s+', ' ', name, flags=re.MULTILINE)
        name = re.sub(r"(^ )|( $)", '', name)
        name = name.split()
        self.access_mode = (name[0]
                            if name[0] in ACCESS_MODES else 'package-private')
        self.implements = (name[name.index('implements') + 1]
                           if 'implements' in name else None)
        self.extends = (name[name.index('extends') + 1]
                        if 'extends' in name else None)
        self.name = name[name.index('class') + 1]


class Function:
    def __init__(self, name: str, signature: str, doc: str):
        self.name = name
        self.signature = signature
        self.docx = doc

    def __eq__(self, other):
        return (self.name == other.name
                and self.signature == other.signature
                and self.docx == other.docx)

    def parse_name(self):
        pass


class Space:
    def __init__(self, parent, namespace: Class or Function,
                 start: int, end: int):
        self.namespace = namespace
        self.parent = parent
        self.children = []
        self.start = start
        self.end = end
        self.last_index = start
        if parent is None:
            self.depth = 1
        else:
            self.depth = parent.depth + 1

    def __str__(self):
        indent = "\t" * (self.depth - 1)
        return f'{indent} {str(self.namespace.name)}\n{indent}' \
               f' {self.namespace.docx}\n'

    def __eq__(self, other):
        return (self.parent == other.parent
                and self.namespace == other.namespace
                and self.children == other.children
                and self.start == other.start
                and self.end == other.end)


def get_blocks(text):
    opened_blocks = []
    blocks = dict()
    for index, symbol in enumerate(text):
        if symbol == '{':
            opened_blocks.append(index)
        elif symbol == '}' and opened_blocks:
            blocks[index] = opened_blocks[-1]
            blocks[opened_blocks.pop()] = index
    return blocks


class Parser:
    def __init__(self, text: str):
        self.result = []
        self.index = 0
        self.pattern_name = r'\s*(?P<name>[^;(*/)\(\)\{\}]+?)' \
                            r'(?P<signature>\([^;\*\{\}]*?\)){0,1}\s*{'
        self.pattern_doc = r'/\*\*(.*?)\*/'
        self.pattern_trash = r'((while|for|if|else if|catch)\s*\(' \
                             r'[^;]*?\)\s*{)|((try|else)\s*{)'
        self.pattern_comments = r'/\*[^*].*?\*/|//.*?\n|".*?"|\'.*?\''
        self.text = self.text_clean_comment(text)
        self.blocks = get_blocks(self.text)
        self.text_cleanup()

    # Удаляем из текста программы все комментарии
    def text_clean_comment(self, text):
        return re.sub(self.pattern_comments, '', text,
                      flags=re.DOTALL | re.MULTILINE)

    # Удаляем лишние фигурные скобки и их содержимое
    def text_cleanup(self):
        trash = []
        start_index = 0
        while True:
            match = re.search(self.pattern_trash, self.text[start_index::],
                              re.MULTILINE)
            if not match:
                break
            start = match.end() - 1 + start_index
            start_index = self.blocks[start]
            trash.append(start)
        if not trash:
            return self.text
        new_text = self.text[:trash[0]:]
        for i, s in enumerate(trash[1::]):
            new_text += self.text[self.blocks[trash[i]] + 1:s:]
        new_text += self.text[self.blocks[trash[-1]] + 1::]
        self.text = new_text
        self.blocks = get_blocks(new_text)

    # Мы пока не смотрим на св-ва, только фигурные скобочки, циклы игнорим
    def get_new_space(self, cur_space: Space) -> Space:
        start, end = self.get_bounds(cur_space)
        if start == -1:
            self.result.append(cur_space)
            return cur_space.parent
        try:
            new_doc = self.get_doc(cur_space, start)
            new_namespace = self.get_name(cur_space)
            if new_namespace.group('signature'):
                new_namespace = Function(new_namespace.group('name'),
                                         new_namespace.group('signature'),
                                         new_doc)
            else:
                new_namespace = Class(new_namespace.group('name'), new_doc)
        except Exception:
            cur_space.last_index = end
            return cur_space

        new_space = Space(cur_space, new_namespace, start, end)
        cur_space.children.append(new_space)
        cur_space.last_index = end
        return new_space

    def get_name(self, cur_space: Space):
        new_namespace = re.search(self.pattern_name,
                                  self.text[cur_space.last_index::],
                                  re.DOTALL | re.MULTILINE)
        if not new_namespace:
            raise Exception("Name not found")
        return new_namespace

    def get_doc(self, cur_space: Space, end: int) -> str:
        new_docs = re.findall(self.pattern_doc,
                              self.text[cur_space.last_index:end:],
                              re.DOTALL | re.MULTILINE)
        return re.sub(r'\s+', ' ', new_docs[-1]) if new_docs else "* No data"

    def get_bounds(self, cur_space: Space) -> tuple:
        start = self.text[cur_space.last_index + 1:cur_space.end:].find('{')
        if start == -1:
            return -1, -1
        start += cur_space.last_index + 1
        end = self.blocks[start]
        return start, end

    def start(self):
        # Ищем главный класс
        start = self.text.find('{')
        if start == -1:
            return
        end = self.blocks[start]

        name = re.search(self.pattern_name, self.text,
                         re.DOTALL | re.MULTILINE)
        docs = re.findall(self.pattern_doc, self.text[:start:],
                          re.DOTALL | re.MULTILINE)
        doc = docs[-1] if docs else "* No data"
        if name.group('signature'):
            namespace = Function(name.group('name'),
                                 name.group('signature'),
                                 doc)
        else:
            namespace = Class(name.group('name'), doc)
        cur_space = Space(None, namespace, start, end)

        # Делаем обход в глубину
        while cur_space is not None:
            cur_space = self.get_new_space(cur_space)

    # # удаление внутренностей функций и полей без доков, принимает
    # # индекс фигурной скобочки
    # def trash_handler(self) -> None:
    #     pass
    #
    # # Принимает индекс конца имени(начала сигнатуры)
    # # и идет назад до запретного символа
    # def get_name_by_index(self) -> str:
    #     i = self.index
    #     disabled_sym = {'/', ';', '}', '{'}
    #     while self.text[i] not in disabled_sym:
    #         i -= 1
    #     return self.text[i+1:self.index:]
    #
    # # принимает индекс начала, возвращает сигнатуру метода
    # def get_signature(self) -> str:
    #     opened = 1
    #     while opened != 0:
    #         pass
    #
    # # Все комментарии вырезаны, позднее if'ы будут вынесены в словарь
    # def automaton(self):
    #     l = len(self.text)
    #     while self.index < l:
    #         if self.text[self.index] == '(':
    #             self.trash_handler()
    #         self.index += 1


def get_struct(file_name) -> list:
    with open(file_name, encoding='utf-8') as f:
        p = Parser(f.read())
    p.start()
    return p.result
