from java_to_struct import Space
import java_to_struct as jts


def get_name_html(name: str):
    if name.startswith("class" or " class" or "public class"):
        return f'<span style="background-color: #AAAAAA"><b>{name}</b></span>'
    return f'<span style="background-color: #AAAAAA">{name}</span>'


def get_doc_html(doc: str):
    return f'<span style="background-color: #CCCCCC">{doc}</span>'


def search_main(space: Space):
    while space.parent is not None:
        space = space.parent
    return space


def write(spaces: list, file):
    for space in spaces:
        file.write('<blockquote>' * (space.depth - 1)
                   + get_name_html(space.namespace.name)
                   + get_doc_html(space.namespace.docx
                                  .replace("*", "<br>"))
                   + '</blockquote>' * (space.depth - 1))
